/*
  Copyright 2019 Rasmus Wennerström <rasmuswennerstrom@gmail.com>

  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

const https = require("https");

const red = text => `\x1b[31m${text}\x1b[0m`;
const green = text => `\x1b[32m${text}\x1b[0m`;
const yellow = text => `\x1b[33m${text}\x1b[0m`;

const printHelp = () =>
  console.log(
    `
 NORDVPN SERVER LIST

 This tool allows you to filter, sort and output information from the NordVPN server API.
 
 You may for instance:

 • Find the server in your country with the lowest load
 • Find servers with specific features (e.g. IKEv2)
 • Find servers within a specific category (e.g. 'Double VPN')

 The tool supports outputting a human readable format, JSON (-j), or a single server property (-p).

 PARAMETERS:

 -f, --filter ${green("[PROP] <OP>[VALUE]")}
 
   Filter servers using the provided ${green("PROP")}erty and ${green("VALUE")}.

   • ${green("[PROP]")}: The property to use when filtering.
   • ${green("[VALUE]")}: The value to use when matching.
   • ${green(
     "<OP>"
   )}: For numerical values, you may use "+" (meaning greater than) 
     and "-" (meaning less than), if omitted, exact matching is used.
   • For strings, case-insensitive regex is used for matching.
   • Arrays are supported (e.g. "supported_features"),
     each element is tested using the above rules.

   E.g: Show servers in Germany with a load under 30, which support IKEv2.

    ${yellow("-f country '^germ' -f load -30 -f supported_features ikev2")}

   Valid property names can be found using -j. 

 -s, --sort ${green("<OP>[PROP]")}

  Sort servers using the provided ${green("PROP")}.
   
   • ${green("[PROP]")}: The property to sort by
   • ${green("<OP>")}: "-" ascending order, "+" for descending order (default)

  E.g. Sort servers by load in ascending order, then by domain.

    ${yellow("-s -load -s domain")}

  Valid property names can be found using -j. 

 -l, --limit ${green("[LIMIT]")}

  Limit the number of servers returned.

   • ${green("[LIMIT]")}: The number of servers to output.

  E.g. To only output one result:

    ${yellow("-l 1")}

 -p, --prop ${green("[PROP]")}

  Only output a single property for each server.

  • ${green("[PROP]")}: The property to output.

  E.g. Limit the result to only output "domain"

    ${yellow("-p domain")}

 -j, --json

  Format the output as JSON, this includes all property names.

  ${red("NOTE:")} This format is not equal to the format returned by the API.

 -h, --help

   this.

 CHEAT SHEET:

  Top 3 servers in ukraine, sorted by load:

   ${yellow("-f country ukraine -s -load -l 3")}

  Find servers with OpenVPN TCP enabled:

   ${yellow("-f supported_feature '^openvpn_tcp$'")}

  Find servers with Double VPN

   ${yellow("-f categories 'Double VPN'")}
`
  );

const getOptions = args => {
  if (/-h|--help/.test(args[0])) {
    return null;
  }

  getOperator = value => {
    if (["+", "-"].includes(value[0])) {
      return value[0];
    }

    return "";
  };

  const parseFilterArg = remainder => {
    const operator = getOperator(remainder[1]);
    const value = remainder[1].slice(operator.length);

    return {
      prop: remainder[0],
      operator,
      value
    };
  };

  const parseSortArg = value => {
    const operator = getOperator(value);
    const prop = value.slice(operator.length);

    return { desc: operator !== "-", prop };
  };

  const filters = [];
  const sorts = [];
  let formatJSON = false;
  let limit = Infinity;
  let outputProp = null;

  for (let i = 0; i < args.length; i++) {
    if (/-f|--filter/.test(args[i])) {
      filters.push(parseFilterArg(args.slice(i + 1)));

      i += 2;
    } else if (/-s|--sort/.test(args[i])) {
      sorts.push(parseSortArg(args[i + 1]));

      i += 1;
    } else if (/-l|--limit/.test(args[i])) {
      limit = Number(args[i + 1]);

      i += 1;
    } else if (/-p|--prop/.test(args[i])) {
      outputProp = args[i + 1];

      i += 1;
    } else if (/-j|--json/.test(args[i])) {
      formatJSON = true;
    } else {
      console.log();
      console.error(red(`!! Unknown parameter '${args[i]}' at index ${i}.`));

      process.exit(1);
      return;
    }
  }

  return { filters, sorts, limit, outputProp, formatJSON };
};

const loadServers = callback => {
  const API_URL = "https://nordvpn.com/api/server";
  let body = "";

  const parseResponse = () => {
    const serverList = JSON.parse(body);

    return serverList.map(server => {
      const { features, ...featureless } = server;

      return {
        ...featureless,

        categories: server.categories.map(cat => cat.name),
        supported_features: Object.keys(server.features)
          .filter(f => server.features[f])
          .sort(),
        unsupported_features: Object.keys(server.features)
          .filter(f => !server.features[f])
          .sort()
      };
    });
  };

  https.get(API_URL, response =>
    response
      .on("data", chunk => (body += chunk))
      .once("end", () => callback(parseResponse()))
  );
};

const featureNames = {
  ikev2_v6: "IKEv2 IPv6",
  ikev2: "IKEv2",
  l2tp: "L2TP",
  openvpn_tcp_v6: "OpenVPN TCP-IPv6",
  openvpn_tcp: "OpenVPN TCP",
  openvpn_udp_v6: "OpenVPN UDP-IPv6",
  openvpn_udp: "OpenVPN UDP",
  openvpn_xor_tcp: "OpenVPN XOR-TCP",
  openvpn_xor_udp: "OpenVPN XOR-UDP",
  pptp: "PPTP",
  proxy_cybersec: "Cybersec",
  proxy_ssl_cybersec: "SSL Cybersec",
  proxy_ssl: "SSL",
  proxy: "Proxy",
  socks: "SOCKS"
};

const listFeatures = features =>
  features
    .filter(f => featureNames[f])
    .map(f => featureNames[f])
    .sort();

const printServer = server => {
  const supportedFeatures = listFeatures(server.supported_features);
  const unsupportedFeatures = listFeatures(server.unsupported_features);

  console.log(`${server.domain} [${server.load} %]`);
  console.log();
  console.log("Categories:");
  console.log(" · " + server.categories.join("\n · "));
  console.log();

  console.log("Features:");
  if (supportedFeatures.length) {
    console.log(green(" + ") + supportedFeatures.join(green("\n + ")));
  }
  if (unsupportedFeatures.length) {
    console.log(red(" - ") + unsupportedFeatures.join(red("\n - ")));
  }

  console.log();
};

const filterList = (servers, filters) => {
  const matchesFilter = (serverValue, filter) => {
    if (serverValue === undefined) {
      return true;
    } else if (serverValue instanceof Array) {
      return serverValue.some(v => matchesFilter(v, filter));
    }

    const serverNum = Number(serverValue);
    const filterNum = Number(filter.value);

    if (isNaN(serverNum) || isNaN(filterNum)) {
      return new RegExp(filter.value, "i").test(serverValue);
    }

    switch (filter.operator) {
      case "+":
        return serverNum > filterNum;
      case "-":
        return serverNum < filterNum;
      default:
        return serverNum === filterNum;
    }
  };

  return servers.filter(server =>
    filters.every(filter => matchesFilter(server[filter.prop], filter))
  );
};

const sortList = (servers, sorts) => {
  for (const sort of sorts) {
    const compare = (left, right) => {
      if (left === undefined || right === undefined) {
        return 0;
      } else if (isNaN(left) || isNaN(right)) {
        const LEFT = left.toString().toUpperCase();
        const RIGHT = right.toString().toUpperCase();

        return sort.desc
          ? RIGHT.localeCompare(LEFT)
          : LEFT.localeCompare(RIGHT);
      }

      return sort.desc ? right - left : left - right;
    };

    // In-place sorts :(

    servers.sort((ls, rs) => compare(ls[sort.prop], rs[sort.prop]));
  }

  return servers;
};

const runProgram = () => {
  const options = getOptions(process.argv.slice(2));

  if (!options) {
    printHelp();

    return 0;
  }

  loadServers(servers => {
    servers = filterList(servers, options.filters);
    servers = sortList(servers, options.sorts);
    servers = servers.slice(0, options.limit);

    if (options.outputProp) {
      servers.forEach(s => console.log(s[options.outputProp]));
    } else if (options.formatJSON) {
      console.log(JSON.stringify(servers, undefined, 2));
    } else {
      servers.forEach(printServer);
    }
  });

  return 0;
};

runProgram();
