# NordVPN CLI tools

**DISCLAIMER:** This project is in no way affiliated with NordVPN or its partners.

This is a tool for managing NordVPN profiles for OpenVPN.

## TL;DR usage:

```bash
sudo nordvpn best-tcp $YOUR_COUNTRY
sudo nordvpn restart
```

![](https://i.imgur.com/l8ljUfD.mp4)


## Features

- Managing of NordVPN client profiles
- Managing your NordVPN credentials for authentication
- Managing the `openvpn-client@nordvpn` service

## Dependencies

- openvpn
- unzip
- wget
- node

## Prerequisites

- A [NordVPN account](https://join.nordvpn.com/order)
- Basic knowledge of how to use the terminal

## Installation

Manual installation:

1. Install the dependencies listed above
2. Save `nordvpn` and `nordvpn-servers.js` to a folder of your choice, e.g `/usr/bin/`
3. Run `chmod +x ./nordvpn` to enable execution of the script

## Basic usage

All commands must be run with root privileges (using for instance `sudo`).

### Getting the server profiles

After installation, start with downloading the NordVPN server profiles.

This is done by running:

```bash
nordvpn update-servers
```

This will download all NordVPN server profiles, and unzip them to `/etc/openvpn/`

_This command can be run at any time to stay up to date, if servers are updated, or added to the NordVPN network._

### Authentication

In order to use the NordVPN network, you must first provide your credentials.

You do this by executing the following command:

```bash
nordvpn auth
```

Then simply follow the instructions in the terminal.

> **NOTE:** The details you provide here will be saved in clear text to `/etc/openvpn/client/auth.txt`.

### Selecting a profile

The suggested way of selecting a profile is via:

```bash
nordvpn best-tcp [country]
```

This will select the server with the least load in the provided country.

For example:

```bash
nordvpn best-tcp ukraine
```

Alternatively, you may use `best-udp` to use VPN over UDP.

### Starting the service

To start the OpenVPN service using your profile, run:

```bash
nordvpn start
```

If you visit [nordvpn.com](https://nordvpn.com/) your status should now say "protected".

### Stopping the service

To stop running the OpenVPN service ("to disconnect"):

```bash
nordvpn stop
```

### Restarting the service

Whenever you change your server profile, you must restart the OpenVPN service:

```bash
nordvpn restart
```

### Running NordVPN on startup

Simply run:

```bash
nordvpn enable
```

To disable the service, run:

```bash
nordvpn disable
```
